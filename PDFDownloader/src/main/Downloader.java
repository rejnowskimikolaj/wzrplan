package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Downloader {
	
	private ArrayList<Group> groups;
	
	public Downloader(){
		this.groups=new ArrayList<>();
	}
	
	public void setGroups(){
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("resources//groups.txt").getFile());
		ArrayList<String> typeList = new ArrayList<>();
		Scanner sc;
		try {
			sc = new Scanner(file);
			while(sc.hasNextLine()){
				typeList.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(String line:typeList){
			String[] groupsArray = line.split(",");
			for(int i=1;i<groupsArray.length;i++){
				groups.add(new Group(groupsArray[i],groupsArray[0]));
			}
		}
	}
	
	public void downloadAll() throws IOException{
		for(Group g:groups){
			String argument = "?sort=4&f1="+g.getName()+"&st="+g.getType();
			downloadPDF(argument,g);
		}
		
		System.out.println("files downloaded :) pzdr gesior");
	}
	public void downloadPDF(String arguments,Group group) throws IOException{
		
		System.out.println("opening connection");
		
		URL url = new URL("http://www.wzr.ug.edu.pl/.pdf/plany.php"+arguments);
		InputStream in = url.openStream();
		FileOutputStream fos = new FileOutputStream(new File("src\\pdf\\"+group.getType()+group.getName()+".pdf"));

		System.out.println("reading from resource and writing to file...");
		int length = -1;
		byte[] buffer = new byte[1024];// buffer for portion of data from connection
		while ((length = in.read(buffer)) > -1) {
		    fos.write(buffer, 0, length);
		}
		fos.close();
		in.close();
		System.out.println("File downloaded");
	}

	public ArrayList<Group> getGroups() {
		return groups;
	}
	
	
}
